# KILI'sLIST
This project was realised with the help of angular and firebase. 
Firebase is a non-SQL cloud database provided by google that offers (near) realtime synchronisation of data as well as good documentation. Furthermore the official `angularfire 2` library was used to interact with the database in the cloud.

## potenial alternatives

Firebase is a solution that offers many features and i in my opinion the best technologie for such a usecase. but of course there are alternatives; one of the would be diff-sync. The problem with diff-snc is that it is still necessary to manualy connect it to a database that would have to be hosted and mainteaned somewhere. when using firebase there is absolutely no need to manualy configure anything.



## Implementation

The datastructure is as follows:

```
export interface Item { 
  id: string,
  createdBy: string,
  number: number,
  item: string,
 }
```

In this version of the shopping list the `createdBy` attribute is not used but a user-specific list is planned.



## CRUD

for creating,  updating and deleting data the built-in functions of `angularfire 2` where used:

````
add(){  
    if (this.auth.user) {


      let MyItem:Item_data = {
        createdBy:this.input_uid,
        item: this.input_item,
        number: this.input_number
      }
      this.itemsCollection.add(MyItem);
      console.log(MyItem)
    }
  }

  deleteItem(it: Item) {
    this.itemsCollection.doc(it.id).delete();
  }


  updateItem(it: Item, name, num) {

  
  if (name != "" && num !=""){
    
    let newItem: Item = {
      id:  it.id,
      createdBy: "",
      number: it.number,
      item: it.item,
    };

    if (name != "") {
      newItem.item = name;
    }
    if (num != ""){
      newItem.number = num;
    }

    this.itemsCollection.doc(it.id).update(newItem);

  } else {
    console.log("you stupid?")
    return;
  }

  }
```

reading data and displaying it on the frontend is done by fething it from firebase and interpolating it into the template.

The fething is done by ustilising the angularfirestore interface:
```
this.itemsCollection = afs.collection<Item>('einkaufsliste');
```
this only makes the content of the object avalable and not the object id. but as the id of each element is needed to perform CRUD operations the next step is to map each object to its id:

```
this.$items = this.itemsCollection.snapshotChanges().map(actions => {
      return actions.map(action => {
        const data_ = action.payload.doc.data() as Item_data;
        
        const id:string = action.payload.doc.id;
        let createdBy:string = data_.createdBy;
        let number:number = data_.number;
        let item:string = data_.item;

        return { id, createdBy , number, item };
      });
    });
```

the `$items` variable is visible in teh template and is iterated through:
```
 <div *ngFor="let item of $items | async">
``` 
Inside of the loop cards are created and filled with the content of the database.
This makes it possible to instantely update the page if a change has ocured as the observable `$item` is automaticaly updated by the API.


## replication strategie
if two users try to modify the same entry at (almost) the same time, the last one to update wins.


# usage
a quick guide on how to use KILI'sLIST

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

