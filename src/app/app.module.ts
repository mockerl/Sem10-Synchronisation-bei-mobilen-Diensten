import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { AngularFireModule } from 'angularfire2';
import { ShowItemsComponent } from './show-items/show-items.component';

import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AngularFirestoreModule } from 'angularfire2/firestore';


export const firebaseConfig = {
  apiKey: "AIzaSyCi-YZIwjekd_KHwg9m7jq3X4VHhjVNzfs",
  authDomain: "angauth-5070f.firebaseapp.com",
  databaseURL: "https://angauth-5070f.firebaseio.com",
  projectId: "angauth-5070f",
  storageBucket: "angauth-5070f.appspot.com",
  messagingSenderId: "42130334729"
};

@NgModule({
  declarations: [
    AppComponent,
    UserProfileComponent,
    ShowItemsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule.enablePersistence(),
    CoreModule, // <-- add core module
    FormsModule,
    HttpModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
