import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../core/auth.service';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore , AngularFirestoreCollection} from 'angularfire2/firestore';
import * as firebase from 'firebase/app';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

export interface Item { 
  id: string,
  createdBy: string,
  number: number,
  item: string,
 }

 export interface Item_data { 
  createdBy: string,
  number: number,
  item: string,
 }

@Component({
  selector: 'app-show-items',
  templateUrl: './show-items.component.html',
  styleUrls: ['./show-items.component.css']
})
export class ShowItemsComponent implements OnInit {

  private itemsCollection: AngularFirestoreCollection<Item_data>;
  public $items: Observable<Item[]>;
  
  public input_item: string = null
  public input_number: number = null
  public input_uid: string = null
  private userID = null


  constructor(public afAuth: AngularFireAuth, private afs: AngularFirestore, private auth: AuthService) {
    this.itemsCollection = afs.collection<Item>('einkaufsliste');
    //this.items = this.itemsCollection.valueChanges();

    this.$items = this.itemsCollection.snapshotChanges().map(actions => {
      return actions.map(action => {
        const data_ = action.payload.doc.data() as Item_data;
        
        const id:string = action.payload.doc.id;
        let createdBy:string = data_.createdBy;
        let number:number = data_.number;
        let item:string = data_.item;

        return { id, createdBy , number, item };
      });
    });
  }

  ngOnInit() {

  }
  
  //queryDB () {
  //  //let thing = this.afs.collection('items', ref => ref.where('uid', '==', this.input_uid))
  //  this.items = this.afs.collection<Item>('item', ref => ref.where('uid', '==', 1)).valueChanges();
  //}

  add(){  
    if (this.auth.user) {


      let MyItem:Item_data = {
        createdBy:this.input_uid,
        item: this.input_item,
        number: this.input_number
      }
      this.itemsCollection.add(MyItem);
      console.log(MyItem)
    }
  }

  deleteItem(it: Item) {
    this.itemsCollection.doc(it.id).delete();
  }

  //updateItem(it: Item) {
  updateItem(it: Item, name, num) {

  
  if (name != "" && num !=""){
    
    let newItem: Item = {
      id:  it.id,
      createdBy: "",
      number: it.number,
      item: it.item,
    };

    if (name != "") {
      newItem.item = name;
    }
    if (num != ""){
      newItem.number = num;
    }

    this.itemsCollection.doc(it.id).update(newItem);

  } else {
    console.log("you stupid?")
    return;
  }

  }

}
